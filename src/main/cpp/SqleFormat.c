//
// Created by Dan on 03.02.2023.
//

#include "SqleFormat.h"

JNIEXPORT jstring JNICALL Java_library_sqliteext_SqleFormat_text(JNIEnv *env, jclass class, jstring value) {
    gchar *sdkValue = GeJNIEnvGetStringUTFChars(env, value, NULL);
    g_autofree gchar *sdkRet = sqle_format_text(sdkValue);
    GeJNIEnvReleaseStringUTFChars(env, value, sdkValue);
    jstring ret = (*env)->NewStringUTF(env, sdkRet);
    return ret;
}

JNIEXPORT jstring JNICALL Java_library_sqliteext_SqleFormat_texts(JNIEnv *env, jclass class, jobjectArray value) {
    g_autolist(gchar) sdkValue = GeJNIEnvJObjectArrayGListGCharArrayTo(env, value);
    g_autofree gchar *sdkRet = sqle_format_texts(sdkValue);
    jstring ret = (*env)->NewStringUTF(env, sdkRet);
    return ret;
}

JNIEXPORT jstring JNICALL Java_library_sqliteext_SqleFormat_ints(JNIEnv *env, jclass class, jintArray value) {
    g_autoptr(GList) sdkValue = GeJNIEnvJIntArrayGListGIntTo(env, value);
    g_autofree gchar *sdkRet = sqle_format_ints(sdkValue);
    jstring ret = (*env)->NewStringUTF(env, sdkRet);
    return ret;
}
