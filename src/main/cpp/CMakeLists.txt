cmake_minimum_required(VERSION 3.10.2)
project(library-sqliteext C)

set(CMAKE_C_STANDARD 99)

file(GLOB C sqlite-ext/*.c)
file(GLOB L */lib/${ANDROID_ABI})
set(l libsqlite3.a)
set(I sqlite3/include)

add_library(library-sqliteext SHARED ${C} SqleMain.c SqleMain.h SqleFormat.c SqleFormat.h SqleInit.c SqleInit.h SQLiteExt.h)
target_link_directories(library-sqliteext PUBLIC ${L})
target_link_libraries(library-sqliteext ${l} library-glibext)
target_include_directories(library-sqliteext PUBLIC ${I} .)
target_compile_definitions(library-sqliteext PUBLIC SQLITE_CORE SQLITE_ENABLE_PREUPDATE_HOOK)
