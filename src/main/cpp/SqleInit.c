//
// Created by Dan on 15.12.2021.
//

#include "SqleInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    sqle_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
