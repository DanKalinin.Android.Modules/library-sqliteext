package library.sqliteext;

import library.java.lang.LjlObject;

public class SqleFormat extends LjlObject {
    public static native String text(String value);
    public static native String texts(String[] value);
    public static native String ints(int[] value);
}
